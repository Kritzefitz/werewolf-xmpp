{-# LANGUAGE OverloadedStrings #-}
module Game.Werewolf.XMPP.Utils ( failEx
                                , simpleBody
                                , report
                                , shlex
                                ) where

import Data.Char (isSpace)
import qualified Data.Text as T
import Data.XML.Types
import Control.Exception (displayException)
import System.IO ( hPutStrLn
                 , stderr
                 )
import System.Exit (exitFailure)
import Network.Xmpp (XmppFailure)

failEx :: String -> IO a
failEx err = do
  hPutStrLn stderr err
  exitFailure

simpleBody :: T.Text -> Element
simpleBody text = Element
                  { elementName = "{jabber:client}body"
                  , elementAttributes = []
                  , elementNodes = [NodeContent $ ContentText text]
                  }

report :: Either XmppFailure a -> IO (Maybe a)
report toCheck = case toCheck of
                   Left e -> do
                       hPutStrLn stderr $ displayException e
                       return Nothing
                   Right a -> return $ Just a

shlex :: String -> [String]
shlex [] = []
shlex ('"':xs) = inQuote '"' xs
shlex ('\'':xs) = inQuote '\'' xs
shlex str@(x:xs)
    | isSpace x = shlex xs
    | otherwise = let (word, rest) = span (not . isSpace) str
                  in word : shlex rest

inQuote :: Char -> String -> [String]
inQuote sep xs = let (part, rest) = span (not . (==sep)) xs
                 in part : shlex (tail rest)
