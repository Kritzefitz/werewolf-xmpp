{-# LANGUAGE OverloadedStrings #-}
module Game.Werewolf.XMPP.RoomMessages ( handleRoomCommand
                                       , joinRoom
                                       , roomUpdates
                                       ) where

import Control.Concurrent.STM (TVar)
import Control.Lens ( (^.)
                    , (.~)
                    , (.=)
                    , at
                    , _Just
                    , _Right
                    , to
                    , use
                    , view
                    )
import Control.Monad ( forever
                     , when
                     )
import Control.Monad.Trans (lift)
import Data.Aeson (eitherDecode)
import qualified Data.ByteString.Lazy.UTF8 as B
import Data.Maybe ( fromJust
                  , maybeToList
                  , isJust
                  )
import qualified Data.Text as T
import Data.XML.Types ( Element(Element)
                      , Name(Name)
                      , attributeText
                      , elementChildren
                      , isNamed
                      )
import Network.Xmpp ( Session
                    , Jid
                    , jidToTexts
                    , dupSession
                    , jidFromTexts
                    , sendPresence
                    , presenceOnline
                    , presenceTo
                    , presencePayload
                    , pullPresence
                    , presenceFrom
                    , Presence
                    , resourcepart
                    , PresenceError
                    , presenceErrorStanzaError
                    , presenceType
                    , PresenceType(Available, Unavailable)
                    , StanzaError(StanzaError)
                    , toBare
                    , MessageType(GroupChat, Chat, Headline)
                    , jidToText
                    , message
                    , messageTo
                    , messageType
                    , messagePayload
                    , sendMessage
                    )
import Network.Xmpp.Internal (NonemptyText(Nonempty))
import System.Exit (ExitCode(ExitSuccess))
import System.IO ( hPutStrLn
                 , stderr
                 )
import System.Process (readProcessWithExitCode)

import Game.Werewolf ( Message(Message)
                     , Response(Response)
                     )

import Game.Werewolf.XMPP.Options
import Game.Werewolf.XMPP.RoomMessages.Interventions
import Game.Werewolf.XMPP.State
import Game.Werewolf.XMPP.Utils

joinRoom :: Session -> Jid -> T.Text -> Werewolf (Maybe T.Text)
joinRoom sess room nick = do
  alreadyJoined <- isJust <$> use (roomState . at room)
  if alreadyJoined
  then return $ Just "I am already in that room."
  else joinRoomReally sess room nick

joinRoomReally :: Session -> Jid -> T.Text -> Werewolf (Maybe T.Text)
joinRoomReally globalSess room nick = do
  sess <- lift $ lift $ dupSession globalSess -- We are gonna receive
                                              -- a lot of stanzas.
  let oJID = setResource room (Just nick)
  result <- lift $ lift $ sendPresence ( presenceOnline
                                         { presenceTo = Just oJID
                                         , presencePayload = [Element "{http://jabber.org/protocol/muc}x" [] []]
                                         }
                                       ) sess
  case result of
    Left e -> return $ Just $ T.pack $ show e
    Right () -> do
                roomeysE <- gatherParticipants sess room oJID
                case roomeysE of
                  Left e -> return $ Just $ "Joining the room failed: " `T.append` e
                  Right roomeys -> do
                                  roomState . at room .= Just roomeys
                                  return $ Just "Entered the room as requested."

handleRoomCommand :: Session -> RoomState -> T.Text -> Jid -> MessageType -> Werewolf ()
handleRoomCommand sess room text' from t = do
    let nick = room ^. myRoomNick
        continue = case t of
                     GroupChat -> T.stripPrefix (nick `T.append` ":") text'
                     Headline -> Nothing
                     _ -> Just text'
    case continue of
      Nothing -> return ()
      Just text ->
          case resourcepart from of
            Nothing -> return ()
            Just player -> do
                     response <- intervene room issueCommand tag player $ parts text
                     sendResponse sess tag response roomJID
    where parts text = shlex $ T.unpack text
          roomJID = toBare from
          tag = jidToText roomJID

roomUpdates :: Settings -> TVar WerewolfState -> Session -> IO ()
roomUpdates _ stateVar sess = runWerewolf run stateVar
    where run = forever $ do
            (presence, (room, nick)) <- bark $ waitForPresenceMaybe (potentialMUCUserMessage) sess
            let action = case presenceType presence of
                           Available -> Just (True, pure ())
                           Unavailable ->
                             Just ( False
                                  , -- The player might not actually
                                    -- be in a game. If they are not,
                                    -- werewolf just sends an error to
                                    -- them, but as they are not there
                                    -- anymore, sendResponse filters
                                    -- it out and  overall nothing
                                    -- happened.
                                    issueCommand tag nick ["quit"] >>=
                                    \r -> sendResponse sess tag r room
                                  )
                             where tag = jidToText room
                           _ -> Nothing
            traverse (\(present, maybeQuit) ->
                         -- It might seem like we should first check
                         -- if we are in the room, but if we are not
                         -- _Just traverses no places, thus the whole
                         -- traversal assigns nothing.
                         roomFromState room . _Just . roomParticipant nick .= present
                         >> maybeQuit
                     ) action

issueCommand :: T.Text -> T.Text -> [String] -> Werewolf Response
issueCommand tag player parts = do
  bark $ print args
  (code, raw, err) <- bark $ readProcessWithExitCode "werewolf" args ""
  if code == ExitSuccess
  then bark $ reportFailure $ mkResponse raw
  else do
    bark $ hPutStrLn stderr $ "Error in werewolf invocation: " ++ err
    return unknownFailureResp
        where reportFailure (Left e) = do
                                  hPutStrLn stderr e
                                  return unknownFailureResp
              reportFailure (Right resp) = return resp
              unknownFailureResp = Response False [Message (Just player) "An internal error occured while communicating to the werewolf engine."]
              mkResponse = eitherDecode . B.fromString
              args = ["--caller", T.unpack player, "--tag", T.unpack tag, "interpret"] ++ parts

sendResponse :: Session -> T.Text -> Response -> Jid -> Werewolf ()
sendResponse _ _ (Response _ []) _ = return ()
sendResponse sess tag (Response ok (Message rcpt text:msgs)) room =
    case rcpt of
      Nothing -> do
        bark $ send $ message { messageTo = Just  $ setResource room Nothing
                              , messageType = GroupChat
                              , messagePayload = [simpleBody text]
                              }
        sendResponse sess tag (Response ok msgs) room
      Just player -> do
            exists <- use (roomFromState room . to (maybe False $ view $ roomParticipant player))
            -- We can only send message to people who are actually there.
            when exists $
              bark $ send $ message { messageTo = Just $ setResource room $ Just player
                                    , messageType = Chat
                                    , messagePayload = [simpleBody text]
                                    }
            sendResponse sess tag (Response ok msgs) room
  where send msg = do
          result <- sendMessage msg sess
          case result of
            Right () -> return ()
            Left e -> hPutStrLn stderr $ show e

gatherParticipants :: Session -> Jid -> Jid -> Werewolf (Either T.Text RoomState)
gatherParticipants sess room oJID =
  lift (lift $ pullPresence sess) >>= either (return . Left . niceError)
  (\presence ->
      maybe recurse
      (\nick ->
          if isOwnPresence presence
          then pure $ Right $ emptyRoom nick
          else (_Right . roomParticipant nick .~ True)
               <$> recurse
      ) $ interesting presence >>= resourcepart
  )
  where recurse = gatherParticipants sess room oJID

isOwnPresence :: Presence -> Bool
isOwnPresence presence =
  or $
  presencePayload presence
  >>= isNamed (mucUserNS "x")
  >>= elementChildren
  >>= isNamed (mucUserNS "status")
  >>= maybeToList . attributeText "code"
  >>= pure . (== "110")

waitForPresenceMaybe :: (Presence -> Maybe a) -> Session -> IO (Presence, a)
waitForPresenceMaybe f sess =
  pullPresence sess >>=
  either (\_ -> waitForPresenceMaybe f sess)
  (\presence -> maybe
                (waitForPresenceMaybe f sess)
                (pure . (,) presence) $
                f presence
  )

niceError :: PresenceError -> T.Text
niceError pErr = showError $ presenceErrorStanzaError pErr
    where showError (StanzaError t c mTxt _) = T.concat $
                                               [ T.pack $ show c
                                               , "("
                                               , T.pack $ show t
                                               , ")"
                                               ] ++ case mTxt of
                                                      Nothing -> []
                                                      Just (_, Nonempty txt) ->
                                                          [ ": ", txt]

setResource :: Jid -> Maybe T.Text -> Jid
setResource jid res = let (local, domain, _) = jidToTexts jid
                      in fromJust $ jidFromTexts local domain res

interesting :: Presence -> Maybe Jid
interesting p = if presenceType p == Available
                then presenceFrom p
                else Nothing

-- | Checks if the message might be from a user in a MUC by seeing if
-- it has a known from address and a resourcepart. If so, it return
-- the potential JID of the room and the nick of the user.
potentialMUCUserMessage :: Presence -> Maybe (Jid, T.Text)
potentialMUCUserMessage presence = do
  from <- presenceFrom presence
  let room = toBare from
  nick <- resourcepart from
  pure (room, nick)

nsHelper :: T.Text -> T.Text -> Name
nsHelper ns local = Name local (Just ns) Nothing

-- Currently not used
--mucNS :: T.Text -> Name
--mucNS = nsHelper "http://jabber.org/protocol/muc"

mucUserNS :: T.Text -> Name
mucUserNS = nsHelper "http://jabber.org/protocol/muc#user"

-- Currently not used
--xmppNS :: T.Text -> Name
--xmppNS = nsHelper "urn:ietf:params:xml:ns:xmpp-stanzas"
