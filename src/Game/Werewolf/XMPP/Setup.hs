module Game.Werewolf.XMPP.Setup ( withConnection
                                , setup
                                , ConnSettings(..)
                                , connSettings
                                ) where

import qualified Data.Text as T
import Control.Monad (void)
import Control.Monad.Trans (lift)
import Control.Monad.Except ( ExceptT
                            , runExceptT
                            )
import Control.Exception ( bracket
                         , displayException
                         )
import Control.Concurrent.STM ( atomically
                              , newTVar
                              )
import Network.Xmpp ( XmppFailure
                    , session
                    , def
                    , endSession
                    , Session
                    , simpleAuth
                    , SessionConfiguration
                    , enableRoster
                    , presenceOnline
                    , sendPresence
                    , dupSession
                    )

import Game.Werewolf.XMPP.Options
import Game.Werewolf.XMPP.RoomMessages
import Game.Werewolf.XMPP.Serve
import Game.Werewolf.XMPP.State
import Game.Werewolf.XMPP.Threads
import Game.Werewolf.XMPP.Utils

data ConnSettings = ConnSettings
    { connHost :: String
    , connUsername :: T.Text
    , connPassword :: T.Text
    }

connSettings :: Settings -> ConnSettings
connSettings setts = ConnSettings
                     { connHost = hostSett setts
                     , connUsername = usernameSett setts
                     , connPassword = passwordSett setts
                     }

-- Nothing means everything went well
withConnection :: ConnSettings -> (Session -> ExceptT XmppFailure IO ()) -> IO (Maybe XmppFailure)
withConnection setts action = do
  bracket open close wrap
      where open = session (connHost setts) auth streamConf
            close (Left _) = return ()
            close (Right sess) = endSession sess
            wrap (Left e) = return $ Just e
            wrap (Right sess) = do
                               result <- runExceptT $ action sess
                               case result of
                                 Left e -> return $ Just e
                                 Right () -> return Nothing
            auth = simpleAuth (connUsername setts) (connPassword setts)

streamConf :: SessionConfiguration
streamConf = def
             { enableRoster = False
             }
               

setup :: Settings -> Session -> IO ()
setup setts sess = do
  stateVar <- atomically $ newTVar emptyState
  result <- sendPresence presenceOnline sess
  case result of
    Right () -> return ()
    Left e -> failEx (displayException e)
  runShutConc $ do
    void $ forkIOShut $ dupSession sess >>= roomUpdates setts stateVar
    lift $ serve setts stateVar sess
