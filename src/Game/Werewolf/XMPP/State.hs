{-# LANGUAGE TemplateHaskell, GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}
module Game.Werewolf.XMPP.State ( RoomState
                                , Werewolf
                                , WerewolfT
                                , WerewolfState
                                , bark
                                , calledOutMBodies
                                , emptyRoom
                                , emptyState
                                , forkWerewolf
                                , iRoomFromState
                                , iRoomParticipant
                                , howl
                                , myRoomNick
                                , runWerewolf
                                , runWerewolfT
                                , roomFromState
                                , roomParticipant
                                , roomParticipants
                                , roomState
                                ) where

import qualified Data.Text as T
import qualified Data.Set as S
import qualified Data.Map.Strict as M
import Control.Monad.IO.Class ( MonadIO
                              , liftIO
                              )
import Control.Monad (void)
import Control.Monad.Trans (MonadTrans, lift)
import Control.Monad.Reader ( ReaderT
                            , MonadReader
                            , runReaderT
                            , ask
                            )
import Control.Monad.State (MonadState, state)
import Control.Concurrent.STM ( TVar
                              , atomically
                              , readTVar
                              , writeTVar
                              )
import Control.Lens ( IndexedLens'
                    , Lens'
                    , at
                    , contains
                    , iat
                    , icontains
                    )
import Control.Lens.TH (makeLenses)
import Network.Xmpp (Jid)

import Game.Werewolf.XMPP.Threads

newtype WerewolfT m a = WerewolfT (ReaderT (TVar WerewolfState) m a)
    deriving (Functor, Applicative, Monad, MonadTrans, MonadIO, MonadReader (TVar WerewolfState))

instance (MonadIO m) => MonadState WerewolfState (WerewolfT m) where
    state f = do
      var <- ask
      liftIO $ atomically $ do
                val <- readTVar var
                let (result, newVal) = f val
                writeTVar var newVal
                return result

type Werewolf = WerewolfT (ShutConc IO)

data WerewolfState = WerewolfState
    { _roomState :: M.Map Jid RoomState
    , _calledOutMBodies :: M.Map Jid Bool
    }

data RoomState = RoomState
  { _myRoomNick :: T.Text
  , _roomParticipants :: S.Set T.Text
  }

$(makeLenses ''WerewolfState)

$(makeLenses ''RoomState)

emptyRoom :: T.Text -> RoomState
emptyRoom = flip RoomState S.empty

iRoomFromState :: Jid -> IndexedLens' Jid WerewolfState (Maybe RoomState)
iRoomFromState roomJID = roomState . iat roomJID

roomFromState :: Jid -> Lens' WerewolfState (Maybe RoomState)
roomFromState roomJID = roomState . at roomJID

iRoomParticipant :: T.Text -> IndexedLens' T.Text RoomState Bool
iRoomParticipant participant = roomParticipants . icontains participant

roomParticipant :: T.Text -> Lens' RoomState Bool
roomParticipant participant = roomParticipants . contains participant

emptyState :: WerewolfState
emptyState = WerewolfState
             { _roomState = M.empty
             , _calledOutMBodies = M.empty
             }

runWerewolfT :: (Monad m) => WerewolfT m a -> TVar WerewolfState -> m a
runWerewolfT (WerewolfT reader) = runReaderT reader

runWerewolf :: Werewolf a -> TVar WerewolfState -> IO a
runWerewolf action st = runShutConc $ runWerewolfT action st

forkWerewolf :: Werewolf () -> Werewolf ()
forkWerewolf action = do
  st <- ask
  void $ lift $ forkIOShut $ runWerewolf action st

-- The way wolfs say something (thus do IO)
bark :: IO a -> Werewolf a
bark = lift . lift

-- The way wolfs instruct other (concurrent) wolfs what to do
howl :: ShutConc IO a -> Werewolf a
howl = lift
