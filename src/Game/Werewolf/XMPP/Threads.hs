{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Game.Werewolf.XMPP.Threads ( forkIOShut
                                  , ShutConc
                                  , runShutConc
                                  , cleanupThreads
                                  ) where

import Data.Foldable (for_)
import Data.IORef ( IORef
                  , modifyIORef
                  , readIORef
                  , newIORef
                  , writeIORef
                  )
import Control.Monad (filterM)
import Control.Monad.Trans (MonadTrans)
import Control.Monad.Reader ( ReaderT
                            , runReaderT
                            , ask
                            )
import Control.Monad.Catch ( MonadMask
                           , finally
                           , MonadCatch
                           , MonadThrow
                           )
import Control.Monad.IO.Class ( MonadIO
                              , liftIO
                              )
import Control.Exception (displayException)
import Control.Concurrent ( ThreadId
                          , forkFinally
                          , killThread
                          )
import Control.Concurrent.MVar ( MVar
                               , newEmptyMVar
                               , putMVar
                               , takeMVar
                               , isEmptyMVar
                               )
import System.IO ( hPutStrLn
                 , hPutStr
                 , stderr
                 )

newtype ShutState = ShutState [(ThreadId, MVar ())]

-- This mostly resembles a state monad, but a state monad looses its
-- state when an exception is thrown in the underlying monad.
newtype ShutConc m a = ShutConc (ReaderT (IORef ShutState) m a)
    deriving (Functor, Applicative, Monad, MonadTrans, MonadIO, MonadCatch, MonadThrow, MonadMask)

forkIOShut :: MonadIO m => IO () -> ShutConc m ThreadId
forkIOShut action = do
  sync <- liftIO $ newEmptyMVar
  tID <- liftIO $ forkFinally action (signalEnd sync)
  addThread tID sync
  return tID
      where signalEnd sigTerm res = do
                      case res of
                        Right _ -> return ()
                        Left e -> do
                                 hPutStr stderr "Exception in thread: "
                                 hPutStrLn stderr $ displayException e
                      putMVar sigTerm ()

-- To prevent keeping all the information about threads that died long ago.
cleanupThreads :: (MonadIO m) => ShutConc m ()
cleanupThreads = do
  ref <- ShutConc ask
  liftIO $ do
            ShutState val <- readIORef ref
            newVal <- filterM (isEmptyMVar . snd) val
            writeIORef ref $ ShutState newVal

addThread :: MonadIO m => ThreadId -> MVar () -> ShutConc m ()
addThread tID endSig = modify $ liftShutState ((tID, endSig):)
    where modify :: MonadIO m => (ShutState -> ShutState) -> ShutConc m ()
          modify f = do
            ref <- ShutConc ask
            liftIO $ modifyIORef ref f

liftShutState :: ([(ThreadId, MVar ())] -> [(ThreadId, MVar())]) -> ShutState -> ShutState
liftShutState f (ShutState xs) = ShutState $ f xs

runShutConc :: (MonadMask m, MonadIO m) => ShutConc m a -> m a
runShutConc action = liftIO (newIORef $ ShutState []) >>= runReaderT (unShutConc run)
    where run = finally action waitExit
          waitExit :: (MonadIO m) => ShutConc m ()
          waitExit = do
            ShutState threads <- ShutConc ask >>= liftIO . readIORef
            for_ threads $ \(tID, termSig) -> do
                       liftIO $ killThread tID
                       liftIO $ takeMVar termSig
            return ()

unShutConc :: ShutConc m a -> ReaderT (IORef ShutState) m a
unShutConc (ShutConc r) = r
