{-# LANGUAGE OverloadedStrings #-}
module Game.Werewolf.XMPP.RoomMessages.Interventions ( Intervention
                                                     , intervene
                                                     ) where

import Control.Applicative ( (<|>)
                           , many
                           , optional
                           )
import Control.Lens ( Prism'
                    , (^..)
                    , (^@.)
                    , asIndex
                    , ifolding
                    , prism'
                    )
import qualified Data.Text as T
import Options.Applicative ( Parser
                           , ParserInfo
                           , argument
                           , command
                           , defaultPrefs
                           , execParserPure
                           , flag'
                           , getParseResult
                           , info
                           , long
                           , option
                           , short
                           , subparser
                           , switch
                           )

import Game.Werewolf ( Message(Message)
                     , Response(Response)
                     )

import Game.Werewolf.XMPP.Options
import Game.Werewolf.XMPP.State

data IntervenableCommand = InterveneStart StartCommand
                         deriving (Show)

data StartCommand = StartCommand !Bool ![T.Text]
                  deriving (Show)

-- We do no generate help messages for interventions
interventionParserInfo :: ParserInfo IntervenableCommand
interventionParserInfo = info interventionParser mempty

interventionParser :: Parser IntervenableCommand
interventionParser = subparser $ mconcat [ command "start" $ fmap InterveneStart startParserInfo ]

startParserInfo :: ParserInfo StartCommand
startParserInfo = info startParser mempty

startParser :: Parser StartCommand
startParser = StartCommand
              <$> switch (short 'h' <> long "help")
              <*> many (argument readText mempty)
              <* optional ( option (pure ()) (short 'e' <> long "extra-roles")
                            <|> flag' () (short 'r' <> long "random-extra-roles")
                          )
              <* optional (option (pure ()) (short 'v' <> long "variant"))

type Intervention = RoomState -> (T.Text -> T.Text -> [String] -> Werewolf Response) -> T.Text -> T.Text -> [String] -> Werewolf Response

intervene :: Intervention
intervene room original tag player parts =
  ( maybe noIntervention
    chooseIntervention $
    getParseResult $ execParserPure defaultPrefs interventionParserInfo parts
  ) room original tag player parts
  where chooseIntervention interventionCommand =
          case interventionCommand of
            InterveneStart start -> checkStart start

noIntervention :: Intervention
noIntervention _ = id

checkStart :: StartCommand -> Intervention
checkStart (StartCommand help players) room =
  -- help does nothing, so we just let it through
  if help
  then id
  else case room ^.. ifolding (\ps -> map (\p -> ps ^@. iRoomParticipant p) players)
            . _False . asIndex of
         [] -> id
         missing ->  \_ _ player _ ->
           pure $ Response False [Message (Just player) $
                                  "I cannot find these players in this room: "
                                   `T.append` T.intercalate ", " missing `T.append` "."
                                 ]

_False :: Prism' Bool ()
_False = prism' (\() -> False) (\b -> if b then Nothing else Just ())
