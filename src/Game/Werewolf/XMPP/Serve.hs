{-# LANGUAGE OverloadedStrings #-}
module Game.Werewolf.XMPP.Serve (serve) where

import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import Data.XML.Types ( isNamed
                      , elementText
                      , Element
                      )
import Control.Monad (forever, void)
import Control.Monad.Trans (lift)
import Control.Monad.State (state)
import Control.Concurrent.STM (TVar)
import Control.Lens ( use
                    , at
                    , (.~)
                    , (^.)
                    )
import Network.Xmpp ( Session
                    , getMessage
                    , messagePayload
                    , sendMessage
                    , messageTo
                    , messageFrom
                    , Jid
                    , message
                    , messageType
                    , MessageType(Chat)
                    , toBare
                    )

import Game.Werewolf.XMPP.Admin
import Game.Werewolf.XMPP.Options
import Game.Werewolf.XMPP.RoomMessages
import Game.Werewolf.XMPP.State
import Game.Werewolf.XMPP.Threads
import Game.Werewolf.XMPP.Utils

serve :: Settings -> TVar WerewolfState -> Session -> IO ()
serve setts st sess = runWerewolf (forever $ serveOne setts sess) st

serveOne :: Settings -> Session -> Werewolf ()
serveOne setts sess = do
  lift $ cleanupThreads
  msg <- lift . lift $ getMessage sess
  let bodies = messagePayload msg >>= isNamed "{jabber:client}body"
      t = messageType msg
  if null $ messagePayload msg >>= isNamed "{urn:xmpp:delay}delay"
  then case messageFrom msg of
         Nothing -> return () -- Nothing we can do here
         Just from ->
             case bodies of
               [] -> return () -- It's like nothing really happened
               [body] -> forkWerewolf $ processBody sess setts from body t >>= respond from sess
               body:_:_ -> do
                           callOutMultipleBodies from >>= respond from sess
                           forkWerewolf $ processBody sess setts from body t >>= respond from sess
  else return ()

respond :: Jid -> Session -> Maybe T.Text -> Werewolf ()
respond _ _ Nothing = return ()
respond from sess (Just text) =
    void $ lift . lift $ sendMessage (message
                                      { messageType = Chat
                                      , messagePayload = [simpleBody text]
                                      , messageTo = Just from
                                      }) sess
             >>= report

processBody :: Session -> Settings -> Jid -> Element -> MessageType -> Werewolf (Maybe T.Text)
processBody sess setts from body t =
  case elementText body of
    [] -> return $ Just "There was no content in your message."
    [text] ->
      use (roomFromState $ toBare from)
      >>= maybe
      (handleAdminCommand sess setts text from)
      (\room -> Nothing <$ handleRoomCommand sess room text from t)
    _:_:_ -> callOutXMLInMessage

callOutMultipleBodies :: Jid -> Werewolf (Maybe T.Text)
callOutMultipleBodies from = do
  shouldSend <- state $ \s -> ( fromMaybe False $ s ^. calledOutMBodies . at from
                              , calledOutMBodies . at from .~ Just True $ s
                              )
  if shouldSend
  then return $ Just "You are sending message with multiple bodies. I will always only process the first body you send."
  else return Nothing

callOutXMLInMessage :: Werewolf (Maybe T.Text)
callOutXMLInMessage = return $ Just "There were XML entities in your command. I don't know how to handle these."
