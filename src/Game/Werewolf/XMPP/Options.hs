{-# LANGUAGE OverloadedStrings #-}
module Game.Werewolf.XMPP.Options ( Arguments
                                  , optSpec
                                  , jidArg
                                  , hostArg
                                  , usernameArg
                                  , passwordArg
                                  , adminsArg
                                  , normalizeArgs
                                  , Settings
                                  , hostSett
                                  , usernameSett
                                  , passwordSett
                                  , adminsSett
                                  , readText
                                  , readJid
                                  ) where

import Data.Maybe (fromMaybe)
import Data.Semigroup ((<>))
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Control.Exception (bracket_)
import System.IO ( hFlush
                 , stdout
                 , hGetEcho
                 , stdin
                 , hSetEcho
                 )
import Options.Applicative ( Parser
                           , ParserInfo
                           , strOption
                           , option
                           , long
                           , short
                           , metavar
                           , help
                           , info
                           , helper
                           , fullDesc
                           , progDesc
                           , header
                           , optional
                           , str
                           , ReadM
                           , readerError
                           , Mod
                           , OptionFields
                           , some
                           , argument
                           , (<|>)
                           )
import Network.Xmpp ( Jid
                    , jidFromText
                    , localpart
                    , domainpart
                    )

import Game.Werewolf.XMPP.Utils

data Arguments = Arguments
    { jidArg :: Jid
    , hostArg :: Maybe String
    , usernameArg :: Maybe T.Text
    , passwordArg :: Maybe T.Text
    , adminsArg :: [Jid]
    } deriving (Show)

-- Arguments after normalizing optionals
data Settings = Settings
    { jidSett :: Jid
    , hostSett :: String
    , usernameSett :: T.Text
    , passwordSett :: T.Text
    , adminsSett :: [Jid]
    }

optionSpec' :: Parser Arguments
optionSpec' = Arguments
              <$> option readJid
                      ( long "jid"
                      <> short 'j'
                      <> metavar "JID"
                      <> help "The JID the bot shall use"
                      )
              <*> optional ( strOption
                             ( long "host"
                             <> metavar "XMPP_HOST"
                             <> help "The XMPP server to connect to"
                             )
                           )
              <*> optional
                      ( textOption
                        ( long "username"
                        <> short 'u'
                        <> metavar "USERNAME"
                        <> help "The username, if different from the local part of the JID"
                        )
                      )
              <*> optional
                      ( textOption
                       ( long "password"
                       <> short 'p'
                       <> metavar "PASSWORD"
                       <> help "The password, if not provided you will be asked for it."
                       )
                      )
              <*> some
                      ( argument readJid
                        ( metavar "JID"
                        <> help "The JIDs that are allowed to give the bot directions"
                        )
                      )

optSpec :: ParserInfo Arguments
optSpec = info (helper <*> optionSpec')
          ( fullDesc
          <> progDesc "Runs the werewolf engine in XMPP chatrooms"
          <> header "werewolf-xmpp - Werewolf over XMPP"
          )

readText :: ReadM T.Text
readText = fmap T.pack str

textOption :: Mod OptionFields T.Text -> Parser T.Text
textOption = option readText

readJid :: ReadM Jid
readJid = do
  result <- jidFromText . T.pack <$> str
  case result of
    Just jid -> return jid
    Nothing -> readerError "The JID could not be parsed"

normalizeArgs :: Arguments -> IO Settings
normalizeArgs args = do
  password <- case passwordArg args of
                Just p -> return p
                Nothing -> getPassword "XMPP Password: "
  user <- case username args of
            Just u -> return u
            Nothing -> failEx "Neither a JID with a localpart nor a explicit username were given."
  return $ Settings
             { jidSett = jidArg args
             , hostSett = xmppHost args
             , usernameSett = user
             , passwordSett = password
             , adminsSett = adminsArg args
             }

username :: Arguments -> Maybe T.Text
username args = usernameArg args <|> localpart (jidArg args)

xmppHost :: Arguments -> String
xmppHost args = fromMaybe (T.unpack . domainpart $ jidArg args) (hostArg args)

getPassword :: T.Text -> IO T.Text
getPassword prompt = do
  T.putStr prompt
  hFlush stdout
  pass <- withEcho False T.getLine
  putChar '\n'
  return pass

withEcho :: Bool -> IO a -> IO a
withEcho echo action = do
  old <- hGetEcho stdin
  bracket_ (hSetEcho stdin echo) (hSetEcho stdin old) action
