{-# LANGUAGE OverloadedStrings #-}
module Game.Werewolf.XMPP.Admin.Subscribe (subscribe) where

import Data.Either (lefts)
import qualified Data.Text as T
import Data.Traversable (for)
import Control.Monad.Trans (lift)
import Control.Exception (displayException)
import Network.Xmpp ( Session
                    , Jid
                    , sendPresence
                    , presenceSubscribed
                    )

import Game.Werewolf.XMPP.State

subscribe :: Session -> [Jid] -> Werewolf (Maybe T.Text)
subscribe sess jids = do
  results <- lift $ lift $ for jids $ \jid ->
             sendPresence (presenceSubscribed jid) sess
  let errors = lefts results
  if null errors
  then return Nothing
  else do
    let errorMessages = map (T.pack . displayException) $ errors
        message = T.unlines $ "The following errors were encountered:"
                  : errorMessages
    return $ Just $ message
