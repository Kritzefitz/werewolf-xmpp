{-# LANGUAGE OverloadedStrings #-}
module Game.Werewolf.XMPP.Admin (handleAdminCommand) where

import Data.Semigroup ((<>))
import qualified Data.Text as T
import Options.Applicative ( info
                           , fullDesc
                           , subparser
                           , command
                           , progDesc
                           , ParserInfo
                           , execParserPure
                           , defaultPrefs
                           , ParseError(ShowHelpText)
                           , ParserResult( CompletionInvoked
                                         , Failure
                                         , Success
                                         )
                           , renderFailure
                           , some
                           , argument
                           , metavar
                           , help
                           , parserFailure
                           , helper
                           )
import Network.Xmpp ( Session
                    , Jid
                    , toBare
                    )

import Game.Werewolf.XMPP.Admin.Subscribe
import Game.Werewolf.XMPP.Options
import Game.Werewolf.XMPP.RoomMessages
import Game.Werewolf.XMPP.State
import Game.Werewolf.XMPP.Utils

data Action = Subscribe [Jid]
            | Help
            | Join Jid T.Text
              deriving (Show)

adminOptSpec :: ParserInfo Action
adminOptSpec = info
               ( subparser $
                 ( command "subscribe"
                   ( info
                     ( helper <*>
                       ( Subscribe <$>
                         ( some $ argument readJid $
                           metavar "JID"
                         <> help "A JID for which to approve subscription."
                         )
                       )
                     )
                   $ fullDesc <> progDesc "Approve subscription for one or more JIDs"
                   )
                 )
               <> ( command "help"
                    ( info
                      ( pure Help)
                      $ fullDesc <> progDesc "Show this help message"
                    )
                  )
               <> ( command "join"
                    ( info
                      ( helper <*>
                        ( Join
                          <$> ( argument readJid
                                ( metavar "JID"
                                <> help "The JID of the room to choin"
                                )
                              )
                          <*> ( argument readText
                                ( metavar "NICK"
                                <> help "The nickname to use in the room"
                                )
                              )
                        )
                      )
                    $ fullDesc <> progDesc "Join a MUC room, which enables people there to play werewolf."
                    )
                  )
               ) $ progDesc "Admin commands"

handleAdminCommand :: Session -> Settings -> T.Text -> Jid -> Werewolf (Maybe T.Text)
handleAdminCommand sess setts text from = if isAdmin from setts
                                    then handleCommand sess text
                                    else return $ Just "I only take direct commands from admins. Write me in a groupchat if you want to play."
                                        
handleCommand :: Session -> T.Text -> Werewolf (Maybe T.Text)
handleCommand sess cmd = do
  let parts = shlex $ T.unpack cmd
      parseResult = execParserPure defaultPrefs adminOptSpec parts
  case parseResult of
    CompletionInvoked _ -> return $ Just "How did you manage to invoke completion?! SERIOUSLY?!"
    Failure failure -> return $ Just $ T.pack $ fst $ renderFailure failure ""
    Success action -> case action of
                        Help -> do
                           let failure = parserFailure defaultPrefs adminOptSpec ShowHelpText []
                           return $ Just $ T.pack $ fst $ renderFailure failure ""
                        Subscribe jids -> subscribe sess jids
                        Join jid nick -> joinRoom sess jid nick

isAdmin :: Jid -> Settings -> Bool
isAdmin jid setts = jid `elem` admins || toBare jid `elem` admins
    where admins = adminsSett setts
