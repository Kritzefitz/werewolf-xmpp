module Main where

import Control.Monad.Trans (lift)
import Control.Exception (displayException)
import System.IO ( hPutStrLn
                 , stderr
                 )
import System.Log.Logger
import Options.Applicative (execParser)

import Game.Werewolf.XMPP.Options
import Game.Werewolf.XMPP.Setup

main :: IO ()
main = do
  updateGlobalLogger "Pontarius.Xmpp" $ setLevel DEBUG
  opts <- execParser optSpec
  settings <- normalizeArgs opts
  result <- withConnection (connSettings settings) $ lift . setup settings
  case result of
    Nothing -> putStrLn "Ending."
    Just e -> hPutStrLn stderr $ displayException e
